<?php

require_once('C:/Users/Bearm/OneDrive/Xampp/htdocs/Assignment1/Model/DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;
    
    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'bjornar',
                'Test1234',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
            $bookList = $this->dbModel->getBookList();
            // Sample tests of book list contents
            $this->assertEquals(count($bookList), 3);
            $this->assertEquals($bookList[0]->id, 1);
            $this->assertEquals($bookList[0]->title, 'Jungle Book');
            $this->assertEquals($bookList[1]->id, 2);
            $this->assertEquals($bookList[1]->author, 'J. Walker');
            $this->assertEquals($bookList[2]->id, 3);
            $this->assertEquals($bookList[2]->description, 'Written by some smart gal.');
    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book->id, 1);
        $this->assertEquals($book->title, 'Jungle Book');
        $this->assertEquals($book->author, 'R. Kipling');
        $this->assertEquals($book->description, 'A classic book.');
    }



    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->getBookById("1'; drop table book;--");
        });
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description" 
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testAddBook()
    {   
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
        
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    //Test adding book without description
    public function testAddBookWithoutDescription(){
        $testValues = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => ''];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
        
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    //Test with special chars
    public function testAddBookEscapingSpecialChars(){
        $testValues = ['title' => "<script>document.body.style.visibility='hidden'</script>",
                       'author' => "<script>document.body.style.visibility='hidden'</script>",
                       'description' => "<script>document.body.style.visibility='hidden'</script>"];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->addBook($book);
        
        // Id was successfully assigned
        $this->assertEquals($book->id, 4);
        
        $this->tester->seeNumRecords(4, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 4,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }
    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
        $book = new Book('title', 'author', 'description', 'ID');
        try{
            $this->dbModel->addBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        } catch(InvalidArgumentException $e){};
    }

    // Tests that adding a book fails mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
        $testValues = ['title' => "",
                       'author' => "a",
                       'description' => "b"];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);

        try{
            $this->dbModel->addBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        } catch(InvalidArgumentException $e){};
    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testModifyBook()
    {
        $testValues = ['title' => "New book",
                       'author' => "Some author",
                       'description' => "Some description"];

        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], '3');
        $this->dbModel->modifyBook($book);
        // Id was successfully assigned
        $this->assertEquals($book->id, 3);
        
        $this->tester->seeNumRecords(3, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 3,
                                              'title' => $testValues['title'],
                                              'author' => $testValues['author'],
                                              'description' => $testValues['description']]);
    }

    public function testModifyBookWithoutDescription(){
        $testValues = ['title' => "New book",
        'author' => "Some author",
        'description' => ""];

        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], '3');
        $this->dbModel->modifyBook($book);
        // Id was successfully assigned
        $this->assertEquals($book->id, 3);

        $this->tester->seeNumRecords(3, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 3,
                               'title' => $testValues['title'],
                               'author' => $testValues['author'],
                               'description' => $testValues['description']]);        
    }
    
    public function testModifyBookSpeciaclCharacters(){
        $testValues = ['title' => "<script>document.body.style.visibility='hidden'</script>",
                        'author' => "<script>document.body.style.visibility='hidden'</script>",
                        'description' => "<script>document.body.style.visibility='hidden'</script>"];

        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], '3');
        $this->dbModel->modifyBook($book);
        // Id was successfully assigned
        $this->assertEquals($book->id, 3);

        $this->tester->seeNumRecords(3, 'book');
        // Record was successfully inserted
        $this->tester->seeInDatabase('book', ['id' => 3,
                               'title' => $testValues['title'],
                               'author' => $testValues['author'],
                               'description' => $testValues['description']]);
    }
    
    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {
        $testValues = ['title' => "<script>document.body.style.visibility='hidden'</script>",
        'author' => "<script>document.body.style.visibility='hidden'</script>",
        'description' => "<script>document.body.style.visibility='hidden'</script>"];

        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], 'NotANumber');
        try{
            $this->dbModel->modifyBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        } catch(InvalidArgumentException $e){};

    }
    
    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {
        $testValues = ['title' => "",
        'author' => "",
        'description' => "<script>document.body.style.visibility='hidden'</script>"];

        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], '4');
        try{
            $this->dbModel->modifyBook($book);
            $this->assertInstanceOf(InvalidArgumentException::class, null);
        } catch(InvalidArgumentException $e){};      
    }
    
    // Tests that a book record can be successfully modified.
    public function testDeleteBook()
    {
        $this->dbModel->deleteBook(2);
        $this->tester->seeNumRecords(2, 'book');
        $this->tester->seeInDatabase('book', ['id' => 1,
        'title' => 'Jungle Book',
        'author' => 'R. Kipling',
        'description' => 'A classic book.']);
        $this->tester->seeInDatabase('book', ['id' => 3,
        'title' => 'PHP & MySQL for Dummies',
        'author' => 'J. Valade',
        'description' => 'Written by some smart gal.']);
        $this->tester->dontSeeInDatabase('book', ['id' => 2,
        'title' => 'Moonwalker',
        'author' => 'J. Walker',
        'description' => '']);
    }
    // Tests that adding a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
        $this->tester->expectException(InvalidArgumentException::class, function() {
            $this->dbModel->deleteBook('a');
        });
    }
}