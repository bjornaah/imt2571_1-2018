<?php
use Codeception\Util\Locator;

class BookCollectionCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // Test to verify that the booklist is displayed as expected
    public function showBookListTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php');
        
        // Book list content
        $I->seeInTitle('Book Collection');
        $I->seeNumberOfElements('table#bookList>tbody>tr', 3);
        // Check sample book values
        $I->see('Jungle Book', 'tr#book1>td:nth-child(2)');
        $I->see('J. Walker', 'tr#book2>td:nth-child(3)');
        $I->see('Written by some smart gal.', 'tr#book3>td:nth-child(4)');
        $I->seeElement('tr#book1>td:first-child>a', ['href' => 'index.php?id=1']);
        $I->seeElement('tr#book2>td:first-child>a', ['href' => 'index.php?id=2']);
        $I->seeElement('tr#book3>td:first-child>a', ['href' => 'index.php?id=3']);
        
        // Add new book form content
        $I->seeElement('form#addForm>input', ['name' => 'title']);
        $I->seeElement('form#addForm>input', ['name' => 'author']);
        $I->seeElement('form#addForm>input', ['name' => 'description']);
        $I->seeElement('form#addForm>input', ['type' => 'submit',
                                              'value' => 'Add new book']);
    }
    
    // Test to verify that the book details page is displayed as expected
    public function showBookDetailsTest(AcceptanceTester $I)
    {
        $I->amOnPage('index.php');
        $I->click(1);
        $this->verifyBookDetails($I, 'Jungle Book', 'R. Kipling', 'A classic book.');
        $I->seeLink('Back to book list','index');
        
        // Buttons for updating and deleting book information
        $I->seeElement('form#modForm>input', ['type' => 'submit',
                                              'value' => 'Update book record']);
        $I->seeElement('form#delForm>input', ['type' => 'submit',
                                              'value' => 'Delete book record']);        
    }
    
    // Test to verify that non-numeric book id's are rejected when requesting book information
    public function invalidBookIdRejectedTest(AcceptanceTester $I)
    {
        $I->amOnPage("index.php?id=1'; drop table book;--");
        $I->seeInTitle('Error Page');        
    }
    
    // Helper function that verifies that the book information on the current page matches the parameter values
    protected function verifyBookDetails(AcceptanceTester $I, String $title, String $author, String $description)
    {
        $I->seeInTitle('Book Details');
        $I->seeElement('form#modForm>input', ['name' => 'title',
                                              'value' => $title]);
        $I->seeElement('form#modForm>input', ['name' => 'author',
                                              'value' => $author]);
        $I->seeElement('form#modForm>input', ['name' => 'description',
                                              'value' => $description]);
    }
   
    // Test to verify that new books can be added. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"A Girl's memoirs", author=>"Jean d'Arc", description=>"Single quotes (') should not break anything"
    //   4. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function successfulAddBookTest(AcceptanceTester $I)
    {
        $tests = array (
            $testValues = ['title' => 'New book',
            'author' => 'Some author',
            'description' => 'Some description'],
    
            //Cheking with blank description
            $testValues1 = ['title' => 'New book',
            'author' => 'Some author',
            'description' => ''],
    
    
            //Cheking that it works with single quotes (')
            $testValues2 = ['title' => "A Girl's memoirs",
            'author' => "Jean d'Arc",
            'description' => "Single quotes (') should not break anything"],
      
    
            //Cheking somthing
            $testValues3 = ['title' => "<script>document.body.style.visibility='hidden'</script>",
            'author' => "<script>document.body.style.visibility='hidden'</script>",
            'description' => "<script>document.body.style.visibility='hidden'</script>"]
            );

        $I->amOnPage('index.php');
        $I->submitForm('#addForm', ['title' => $tests[0]['title'], 
                                    'author' => $tests[0]['author'],
                                    'description' => $tests[0]['description']]);

        // Getting booklist with new book added as ID:4
        $I->seeInTitle('Book Collection');
        $I->seeNumberOfElements('table#bookList>tbody>tr', 4);
        $I->see('ID: 4');
        $I->seeElement('tr#book4>td:first-child>a', ['href' => 'index.php?id=4']);
        $I->see($tests[0]['title'], 'tr#book4>td:nth-child(2)');
        $I->see($tests[0]['author'], 'tr#book4>td:nth-child(3)');
        $I->see($tests[0]['description'], 'tr#book4>td:nth-child(4)');
        $I->seeLink('4','index.php?id=4');
        //Cheking with blank description
        $I->submitForm('#addForm', ['title' => $tests[1]['title'], 
        'author' => $tests[1]['author'],
        'description' => $tests[1]['description']]);

        $I->seeNumberOfElements('table#bookList>tbody>tr', 5);
        $I->see('ID: 5');
        $I->seeElement('tr#book5>td:first-child>a', ['href' => 'index.php?id=5']);
        $I->see($tests[1]['title'], 'tr#book5>td:nth-child(2)');
        $I->see($tests[1]['author'], 'tr#book5>td:nth-child(3)');
        $I->see($tests[1]['description'], 'tr#book5>td:nth-child(4)');
        $I->seeLink('5','index.php?id=5');
        //Cheking that it works with single quotes (')
        $I->submitForm('#addForm', ['title' => $tests[2]['title'], 
        'author' => $tests[2]['author'],
        'description' => $tests[2]['description']]);

        $I->seeNumberOfElements('table#bookList>tbody>tr', 6);
        $I->see('ID: 6');
        $I->seeElement('tr#book6>td:first-child>a', ['href' => 'index.php?id=6']);
        $I->see($tests[2]['title'], 'tr#book6>td:nth-child(2)');
        $I->see($tests[2]['author'], 'tr#book6>td:nth-child(3)');
        $I->see($tests[2]['description'], 'tr#book6>td:nth-child(4)');
        $I->seeLink('6','index.php?id=6');

        //Cheking somthing
        $I->submitForm('#addForm', ['title' => $tests[3]['title'], 
        'author' => $tests[3]['author'],
        'description' => $tests[3]['description']]);

        $I->seeNumberOfElements('table#bookList>tbody>tr', 7);
        $I->see('ID: 7');
        $I->seeElement('tr#book7>td:first-child>a', ['href' => 'index.php?id=7']);
        $I->see($tests[3]['title'], 'tr#book7>td:nth-child(2)');
        $I->see($tests[3]['author'], 'tr#book7>td:nth-child(3)');
        $I->see($tests[3]['description'], 'tr#book7>td:nth-child(4)');
        $I->seeLink('7','index.php?id=7');
        
   }
    
    // Test to verify that adding a book fails if mandatory fields are missing
    public function addBookWithoutMandatoryFieldsTest(AcceptanceTester $I)
    {
        //Check for fail without title
        $testValues = ['title' => "",
        'author' => "Some Author",
        'description' => ""];
        $I->amOnPage('index.php');
        $I->submitForm('#addForm', ['title' => $testValues['title'], 
        'author' => $testValues['author'],
        'description' => $testValues['description']]);
        $I->seeInTitle("Error Page");
        
        //Check for fail without author
        $I->click('Back to book list');
        $testValues = ['title' => "Soe Aumthor",
        'author' => "",
        'description' => ""];
        $I->submitForm('#addForm', ['title' => $testValues['title'], 
        'author' => $testValues['author'],
        'description' => $testValues['description']]);
        $I->seeInTitle("Error Page");
        
    }
    
    // Test to verify that book records can be modified successfully. Four cases should be verified:
    //   1. title=>"Different title", author=>"Different Author", description=>"Different description"
    //   2. title=>"Different title", author=>"Different Author", description=>""
    //   3. title=>"A Girl's memoirs", author=>"Jean d'Arc", description=>"Single quotes (') should not break anything"
    //   4. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function successfulModifyBookTest(AcceptanceTester $I)
    {   
        $tests = array (
        $testValues = ['title' => 'Different title',
        'author' => 'Different Author',
        'description' => 'Different description'],

        //Cheking with blank description
        $testValues1 = ['title' => 'Different title',
        'author' => 'Different Author',
        'description' => ''],


        //Cheking that it works with single quotes (')
        $testValues2 = ['title' => "A Girl's memoirs",
        'author' => "Jean d'Arc",
        'description' => "Single quotes (') should not break anything"],
  

        //Cheking somthing
        $testValues3 = ['title' => "<script>document.body.style.visibility='hidden'</script>",
        'author' => "<script>document.body.style.visibility='hidden'</script>",
        'description' => "<script>document.body.style.visibility='hidden'</script>"]
        );
        //Loops thorug all the test in $tests
     for($x = 0; $x < 4; $x++){
        $I->amOnPage('index.php?id=2');
        $I->submitForm('#modForm', [ 'title' => $tests[$x]['title'], 
                                     'author' => $tests[$x]['author'],
                                     'description' => $tests[$x]['description']]); 
        $I->seeInTitle('Book Collection');
        $I->see($tests[$x]['title'], 'tr#book2>td:nth-child(2)');
        $I->see($tests[$x]['author'], 'tr#book2>td:nth-child(3)');
        $I->see($tests[$x]['description'], 'tr#book2>td:nth-child(4)');
        $I->seeLink('2','index.php?id=2');
     }

    }

    
    // Test to verify that modifying a book fails if mandatory fields are missing
    public function modifyBookWithoutMandatoryFieldsTest(AcceptanceTester $I)
    {
        $testValues = ['title' => "",
                       'author' => "Some Author",
                       'description' => ""];
        $I->amOnPage('index.php?id=2');
        $I->submitForm('#modForm', [ 'title' => $testValues['title'], 
                                     'author' => $testValues['author'],
                                     'description' => $testValues['description']]); 
        $I->seeInTitle('Error Page');
    }
    
    // Test to verify that deleting a book succeeds.
    public function successfulDeleteBookTest(AcceptanceTester $I)
    {
         $I->amOnPage('index.php?id=2');
         $I->click('Delete book record');
         $I->amOnPage('index.php');
         $I->dontSee('ID: 2');
    }
    
    // Test to verify that deleting a book with non number id fails.
    public function deleteBookWithInvalidIdTest(AcceptanceTester $I)
    {
         $I->amOnPage('index.php?id=2');
         $I->submitForm('#delForm', [ 'op' => 'del', 
                                      'id' => 'A']);
         $I->seeInTitle('Error Page');
    }
}