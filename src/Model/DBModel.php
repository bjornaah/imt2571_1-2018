<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            $this->db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'bjornar',
                'Test1234',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        //PDO query all the rows in book
        $stmt = $this->db->query('SELECT * FROM book');
        //Fetches the table book from test database
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        //Creates a book object in booklist with the values from the database
        foreach($res as $row){
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
        }
        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
        if(is_Numeric($id)){
            $stmt = $this->db->prepare('SELECT * FROM book where id = :id');
            $stmt->bindValue(':id', $id);
            $stmt->execute();
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
            }
            return $book;
        } else throw new InvalidArgumentException;
    }
    
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
        self::verifyBook($book);
        //Inserts book to database
        $stmt = $this->db->prepare("INSERT INTO book (title, author, description)
                                    VALUES(:title, :author, :description)");
        //Binds the values to placeholders
        $stmt->bindValue(':title', $book->title);
        $stmt->bindValue(':author', $book->author);
        $stmt->bindValue(':description', $book->description);
        $stmt->execute();
        //Adds ID
        $book->id = $this->db->lastInsertId();
    }

    /** Modifies data related to a book in the collection.
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
        
        self::verifyBook($book);
        $stmt = $this->db->prepare("UPDATE book SET title =:title, author = :author, description = :description  WHERE id = :id");
        $stmt->bindValue(':id', $book->id);
        $stmt->bindValue(':title', $book->title);
        $stmt->bindValue(':author', $book->author);
        $stmt->bindValue(':description', $book->description);
        if(!$stmt->execute()){
            throw new InvalidArgumentException($e);
        }  
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
        if(is_numeric($id)){
        $stmt = $this->db->prepare("DELETE FROM book WHERE id = :id");
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        } else throw new InvalidArgumentException("Invalid ID");
    }
}
